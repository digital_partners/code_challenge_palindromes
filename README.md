# Code Challenge - Palindromes

## Installation
Make sure you have node.js installed.

1. Clone this repository
2. Go to the directory and run: npm install
3. Run application: npm run watch


## Challenge
Complete the function "isPalindrome" that takes a string as parameter and checks if the string is a palindrome. 
